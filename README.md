# README #

Ce dépôt est destiné à héberger les sources permettant d'établir les
consignes fournies aux étudiants pour leur stage.

### À quoi sert ce dépôt ? ###

* Il centralise tous les sources des consignes.
* Il peut être utilisé par tous les enseignants du département
  informatique : il suffit de demander d'être ajouté comme
  contributeur.
* Il peut également être utilisé par les étudiants souhaitant
  récupérer un squelette de document LaTeX leur permettant d'écrire
  leur propre rapport (aucun support ne sera cependant fourni sur les
  questions relatives à ce langage, de nombreux tutoriels sont
  disponibles sur Internet).

### Comment contribuer ? ###

* Récupérer les sources.
* Disposer d'une distribution LaTeX incluant XeLaTeX.
* Éditer les modifications et les pousser vers ce dépôt (ou faire un
  pull request).
* Comme dans tous les projets collaboratifs, il est préférable de se
  coordonner avec l'ensemble des contributeurs avant d'effectuer des
  modifications.

### Remarques ###

* Le résultat PDF est également dans le dépôt. Cela permet aux
  personnes souhaitant le récupérer, mais ne disposant pas ou ne
  connaissant pas LaTeX, de pouvoir le faire.
  Il ne faut donc pas en conclure que l'inclusion du PDF dans le dépôt
  est une bonne pratique : ça n'est pas le cas.

### Qui contacter en cas de problème ? ###

* Le propriétaire du dépôt ou un de ses administrateurs 
