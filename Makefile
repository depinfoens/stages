TEXENGINE = latexmk -xelatex
BIBENGINE = bibtex

TARGETNAME  = notice_stages
EXAMPLENAME  = test_rapport

all: normal

normal: $(EXAMPLENAME).pdf $(TARGETNAME).pdf

$(EXAMPLENAME).pdf: $(EXAMPLENAME).tex
	$(TEXENGINE) $(EXAMPLENAME).tex

$(TARGETNAME).pdf: $(TARGETNAME).tex $(TARGETNAME).bib stage.png
	$(TEXENGINE) $(TARGETNAME).tex
	$(BIBENGINE) $(TARGETNAME)
	$(TEXENGINE) $(TARGETNAME).tex

clean:
	/bin/rm -f *~ *.dvi *.log *.aux *.flg *.tmp *.ch *.bbl *.blg *.bat *.lof *.toc *.idx *.ind *.ilg *.out *.snm *.nav *.fls *.fdb_latexmk

