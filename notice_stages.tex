% Type de document à produire, options
\documentclass[12pt]{article}

% La gestion des fontes, sachant que ce document est sensé
% être compilé au moyen de XeLaTeX
\usepackage{fontspec,xltxtra}

% Pour permettre la redéfinition des dimensions 
\usepackage[a4paper]{geometry}

% Pour le support du français
\usepackage[francais]{babel}

% Les polices à utiliser
% (installer le package ttf-mscorefonts-installer sous Linux)
\newcommand{\fontsphil}{
  \defaultfontfeatures{Mapping=tex-text}
  \defaultfontfeatures{Ligatures=TeX}
  \setmainfont{Linux Libertine O}
  \setsansfont{Liberation Sans}
  \setmonofont[Scale=.9]{Ubuntu Mono}
  \newfontfamily{\hwfont}{Comic Sans MS}
  \newfontfamily{\fatfont}{Arial Black}
  \newfontfamily{\authorfont}{Liberation Serif}
  \newfontfamily{\spacefont}{Liberation Mono}
  \fontspec[SmallCapsFont={Arial Black}]{Arial Black}
}
\fontsphil

% Gestion des espaces
\usepackage{xspace}

% Des polices pour quelques caractères spéciaux
% Définition de macros relatives
\usepackage{wasysym}
\usepackage{pifont}
\usepackage{fontawesome}
\newcommand{\soluce}{\faHandORight\xspace}

% Gestion des images
\usepackage{graphicx}

% Gestion des couleurs
\usepackage{color}
\definecolor{fondsection}{rgb}{.9,.9,.9}

% Gestion des URL et liens hypertexte
\usepackage{hyperref}

% Pour inclure des PDF
\usepackage{pdfpages}

% Configuration des dimensions
\geometry{%
  left=20mm,width=165mm,
  top=15mm,height=257mm,
  footskip=15mm}

% Pour éviter les veuves et les orphelines
\widowpenalty=10000
\clubpenalty=10000

% Les macros
\newcommand{\oula}{(\textbf{???})}
\newcommand{\cffig}[1]{[Fig.~\ref{fig:#1}]}

%======================================================================
% D É B U T   D U   D O C U M E N T
%======================================================================

\begin{document}


\thispagestyle{empty}
\begin{center}
  \includegraphics[width=12cm]{Logo_IUT-UL_dept_info.png}\\[8cm]
  {\huge{\textbf{Le rapport de stage et la soutenance}}}\\[1cm]
  {\Large{Consignes}}\\[1cm]
\end{center}


\newpage


\thispagestyle{empty}
\tableofcontents


\newpage


\section{Composition du mémoire}

\subsection{Liste des éléments obligatoires}

La structure attendue pour les rapports est la suivante :
\begin{itemize}
\item
  couverture,
\item 
  page blanche,
\item
  page de titre,
\item
  remerciements,
\item 
  table des matières \footnote{L'annexe~\ref{sec:wys} indique la
    marche à suivre pour qu'elle soit générée automatiquement.} (pour
    rappel, la table des matières \textbf{ne figure pas} dans la table
    des matières, \emph{i.e.} elle-même),
\item
  introduction (\textbf{début de la numérotation des titres}),
\item
  corps du mémoire,
\item
  conclusion,
\item
  bibliographie.
\end{itemize}


\subsection{Liste des éléments facultatifs}

Suivant les rapports, certains éléments peuvent (voire doivent) être
ajoutés :
\begin{itemize}
\item
  glossaire,
\item
  table des annexes,
\item
  annexes.
\end{itemize}


\subsection{Remarques générales}

Du point de vue de la disposition, prévoyez une nouvelle page par
élément (une pour l’introduction, une pour la conclusion, etc.) ;
aérez la présentation de votre texte mais évitez les phrases veuves et
orphelines, c'est-à-dire les lignes isolées ou désolidarisées de leur
paragraphe soit en bas soit en haut de page ou les titres en bas de
page.


\section{Couverture et page de titre}

Un exemple type de page de titre est présenté~\cffig{gard}.
Il faut dans tous les cas y retrouver le nom et l'adresse de l'IUT, le
nom de votre département, l'intitulé de votre formation, vos nom et
prénom, le nom de l'entreprise et naturellement le titre du rapport.

\begin{figure}[h!]
  \centering
  \fbox{\includegraphics[scale=.65]{test_rapport.pdf}}
    \caption{Un exemple de page de titre.}
  \label{fig:gard}
\end{figure}

Les logos officiels de l'IUT peuvent être récupérés sur l'ENT :
choisir le menu «~Composante~», sélectionner «~Intranet IUT NC~»,
aller dans la section «~Communication~» et enfin dans l'entrée «~Logos
\& Charte graphique~».


\section{Rédaction du mémoire}

\subsection{Le fond}

\subsubsection{Introduction}

Elle comporte au moins une présentation générale du travail demandé
et l’annonce du plan.
Le lecteur doit ainsi en quelques phrases avoir une idée générale de
ce qui est présenté dans le rapport et de la façon dont s'articule son
organisation.

\subsubsection{Corps du mémoire}

Votre mémoire doit comporter les parties suivantes :
\begin{itemize}
\item
  \textbf{Une présentation de l’entreprise}\footnote{Pour ce qui est
    de l’organisation, du C.A., de l’historique même de l’entreprise,
    présentez ces éléments en annexe en recourant à des organigrammes,
    tableaux.} :
  \begin{itemize}
  \item 
    N’évoquez dans le rapport que les aspects qui expliquent la raison
    d’être de votre sujet et le justifient (vous pouvez évoquer, le
    cas échéant, pourquoi vous avez choisi cette entreprise).
    On doit comprendre dans ces aspects présentatifs de l’entreprise
    en quoi elle a un besoin par rapport au sujet (\emph{i.e.} les
    éléments contextuels qui préexistent au sujet)\footnote{Cette
      orientation de la présentation doit d’ailleurs faire apparaître un
      recul par rapport au monde de l’entreprise et de ses besoins en
      services et prestations informatiques.}.
  \item 
    Présentez le service informatique (organisation personnel,
    environnement, matériel, etc.) de manière précise puisque cela
    conditionne --- et de fait justifie --- les choix opérés.
  \item 
    Décrivez précisément votre mission (base du sujet si elle existe,
    les objectifs fixés).
  \end{itemize}
\item 
  \textbf{La présentation du travail réalisé} :
  \begin{itemize}
  \item 
    Situez dans la chronologie du projet à quel(s) moment(s) vous êtes
    intervenu(e) (à préciser, dans le rapport, plutôt en énonçant
    la/les étape(s) concernée(s)\footnote{Lors de la soutenance, vous
      pourrez intégrer un schéma.}.
  \item 
    Faites un planning~\cffig{gantt} (type diagramme de Gantt) en
    précisant les étapes et les phases de votre stage (à placer plutôt
    dans la partie «~Annexes~» --- voir annexe~\ref{sec:gantt} --- si
    le diagramme n'est pas assez lisible dans la partie principale de
    votre rapport).
    \begin{figure}[ht]
      \centering
      \includegraphics[width=\linewidth]{stage.png}
      \caption{Un exemple de diagramme de Gantt.}
      \label{fig:gantt}
    \end{figure}
  \item 
    Le contenu porte sur l’analyse et la présentation de la
    \textbf{conception} et de la \textbf{réalisation} de vos tâches
    dans leur déroulement (suivre et présenter leur progression).
    Soulignez les choix opérés (à justifier), les problèmes rencontrés
    et les solutions envisagées pour les résoudre.
    Sachez \textbf{prendre du recul} pour analyser vos choix ou les paramètres
    inhérents au sujet ou au contexte.
  \item 
    Le plan pourra être chronologique ou thématique (soyez attentifs à
    l'intitulé pour chaque phase de votre travail).
    Veillez à donner des \textbf{titres explicites} : le sommaire est
    un document informatif en soi qui doit permettre à un lecteur,
    quel qu’il soit, d’avoir une idée de l’ensemble des aspects
    traités dans votre rapport\footnote{On pourrait presque considérer
      que l’ensemble des titres du sommaire fasse un résumé de votre
      rapport.}.
    Rédigez des \textbf{transitions} entre les grandes parties.
  \end{itemize}
\end{itemize}

\subsubsection{Conclusion}

Elle dresse un bilan des points traités et aborde les apports sur le
plan personnel et professionnel du stage.
\textbf{Notez bien} : l’introduction et la conclusion se rédigent
lorsque le corps du mémoire est achevé.


\subsection{La forme}

\subsubsection{Conventions propres au département informatique}

Afin d'uniformiser les rapports et de cadrer la façon dont ils sont
rédigés, les conventions suivantes sont à respecter :
\begin{itemize}
\item 
  la longueur du mémoire (introduction, corps et conclusion) doit être
  de 30 à 40 pages,
\item 
  le texte doit être sur une seule colonne et justifié (alignement
  gauche et droite),
\item 
  la police à utiliser doit être de type « Times New Roman » en taille
  12 pour le texte (elle peut être différente pour les titres et
  intertitres),
\item 
  en principe, définir un interligne de 1.5 (contrairement à cette
  notice donc, qui est en interligne 1),
\item 
  il faut numéroter les pages de $1$ à $n$ sur l’ensemble du
  mémoire. Cette numérotation commence à la page titre mais n’apparaît
  qu'à partir de l’introduction,
\item  
  il ne faut ni en-tête ni de pied de pages (en dehors de la
  numérotation des pages donc).
\end{itemize}

\subsubsection{Expression}

L'expression dépend de l'aisance de l'auteur pour la rédaction de son
rapport.
Ceci étant, quelle que soit cette aisance, certaines règles doivent
respectées :
\begin{itemize}
\item 
  L'expression doit être soignée (attention à la syntaxe, à la
  grammaire, à la ponctuation, aux connecteurs).
  \textbf{Utilisez des outils de correction automatiques (qui ne sont
    que des assistants, attention aux suggestions) et faites vous
    relire.}
\item 
  Une phrase ne peut jamais constituer un paragraphe, et un paragraphe
  renvoie à une idée développée sur plusieurs lignes.
\item 
  N’interpellez pas le lecteur.
\item 
  Évitez les phrase trop longues, les formes approximatives (plus ou
  moins, de nombreux, etc.).
\item 
  Utilisez un vocabulaire professionnel et adapté au domaine
  concerné\footnote{Prévoir des notes de bas de page ou un glossaire
  (si plus de 10 mots) pour les explications de vocabulaire.}.
\item 
  Utilisez correctement les signes de ponctuation :\\
  \begin{tabular}{|l|p{10.87cm}|}
    \hline
    point & fin d’une phrase\\
    point-virgule & séparation de deux points qui restent liés à une même idée\\
    virgule & énumération de termes de même nature grammaticale, isolement
              syntaxique d’un terme\\
    deux points & introduction d’une énumération, d’une explication\\
    tiret cadratin (---) & introduction d’une réflexion en rapport
                            avec le texte\\
    parenthèses & inclusion d’une explication, d’une précision\\
    point d’exclamation & visualisation d'intonation (exclamation, interjection)\\
    point d’interrogation & visualisation d'intonation (interrogation)\\
    points de suspension & visualisation d'intonation (attente d'une suite)\\
    \hline
  \end{tabular}
  
  \textbf{Les points d’exclamation et les points de suspension sont à éviter
    dans ce genre de documents.}
\item
  Ne mettez \textbf{jamais} de point («~.~») à la fin des titres.
\item 
  Respectez également les règles d’espacement par rapport aux signes
  de ponctuation :
  \begin{itemize}
  \item 
    pour les signes simples («~,~», «~.~») : pas d’espace avant et une
    espace\footnote{Dans ce sens le substantif «~espace~» est de genre
    féminin (« une espace »).} après,
  \item
    pour les signes doubles («~;~», «~:~», «~!~», «~?~») une espace
    \emph{insécable} avant et une espace après,
  \item
    pas d’espace après les parenthèses, crochets et accolades
    ouvrantes, pas d'espace non plus devant leur contrepartie
    fermante,
  \item
    une espace \emph{insécable} après un guillemet ouvrant («) et une
    autre espace \emph{insécable} avant un guillemet fermant (»).
  \end{itemize}
\item
  Ne confondez d'ailleurs pas les guillemets français («~») avec les
  guillemets anglo-saxons ("~").
\item
  Les majuscules doivent être accentuées le cas échéant.
\item 
  Les caractères gras sont préférables au soulignement (plutôt réservé
  aux liens Internet), l’italique est à réserver aux citations, aux
  locutions étrangères et aux définitions.
\item
  Attention à la structuration du document :
  \begin{itemize}
  \item 
    un document ne doit pas contenir de titre «~orphelin~» à son
    niveau hiérarchique.
    Ainsi, si un paragraphe §~3.1.1 est présent, il faut
    impérativement qu'il y ait au moins un paragraphe §~3.1.2 (s'il
    n'y en a pas, il faut alors fusionner le §~3.1.1 au sein du
    §~3.1),
  \item 
    dans la même veine, il faut (idéalement) que la profondeur des
    titres soit uniforme au moins au sein d'un même chapitre (ne pas
    avoir de §~3.1.1.1.5 s'il n'y a pas de sous-section en dessous du
    §~3.2),
  \item
    et enfin limiter de toutes façons la profondeur hiérarchique des
    titres (3 niveaux de titres sur un document d'une quarantaine de
    pages est \emph{a priori} bien suffisant).
  \end{itemize}
\item
  D'autres détails peuvent être trouvés dans des ouvrages tels
  que~\cite{Jacquenod1993ponctuation} ou des sites
  comme~\cite{WikiTypo}.
\end{itemize}

\subsubsection{Tableaux, figures et images, pages écran}

Ils doivent être numérotés, légendés et commentés.
Il faut en indiquer la source.
Ne saturez pas votre rapport de pages écran, elles ne doivent être là
que pour illustrer le propos et doivent obligatoirement être
commentées.

\subsubsection{Citations, les références et la bibliographie}

Les citations servent à illustrer un propos ou à le compléter, elles
doivent donc être en rapport avec le texte et être exploitées.
Il faut en indiquer précisément les références, comme ceci par
exemple : «~selon Jacquenod~\cite{Jacquenod1993ponctuation}, ...~».

Indépendamment des citations, un rapport doit recenser toutes les
sources documentaires utilisées lors de sa rédaction.
Cela se fait au moyen de références (comme ici~\cite{biboff})
renvoyant à la bibliographie du rapport.
La présentation de cette bibliographie est normalisée en fonction du
type de ressource considérée :
\begin{itemize}
\item 
  Pour les \emph{monographies} (livres) :
  \begin{itemize}
  \item 
    \textsc{Nom} et prénom de l’auteur, \emph{titre}, éditeur (lieu si
    possible), année, nombre de pages si possible.
  \item 
    Exemples :
    \begin{itemize}
    \item
      avec un auteur~\cite{mono1},
    \item
      avec deux auteurs~\cite{mono2},
    \item
      avec plus de deux
      auteurs~\cite{Goossens:1994:LC:561206}\footnote{Le «~\emph{et
      al.}~» vient du latin \emph{et alii} («~et autres~»).},
    \item
      en indiquant une page particulière~\cite[page 42]{mono1}.
    \end{itemize}
  \end{itemize}
\item
  Pour les \emph{périodiques} :
  \begin{itemize}
  \item 
    \textsc{Nom} et prénom de l'auteur, titre, \emph{nom du journal},
    date, volume (numéro), pages utilisées («~: n--m~»).
  \item
    Exemples :
    \begin{itemize}
    \item
      une revue scientifique~\cite{revsci},
    \item
      une revue grand public~\cite{svm}.
    \end{itemize}
  \end{itemize}
\item 
  Pour les \emph{sites Internet} :
  \begin{itemize}
  \item 
    Nom du site, \textsc{Nom} et prénom de l'auteur si possible,
    titre, date de consultation, URL.
  \item
    Exemples :
    \begin{itemize}
    \item
      avec auteur~\cite{lozano},
    \item
      sans auteur~\cite{WikiTypo}.
    \end{itemize}
  \end{itemize}
\item 
  Pour les \emph{rapports} (de stage typiquement) :
  \begin{itemize}
  \item 
    \textsc{Nom} et prénom de l’auteur, titre, date, bibliothèque,
    diplôme, établissement, nombre total de pages si possible.
  \item
    Exemple : un rapport de stage~\cite{gerard}.
  \end{itemize}
\end{itemize}

\medskip

Pour aller plus loin, il existe des ressources très complètes
précisant les modalités de présentation de la
bibliographie~\cite{biboff}.
Remarque finale : si plusieurs entrées bibliographiques sont relatives
au même auteur et à la même année, il est d'usage d'utiliser des
minuscules pour compléter la référence~\cite{multi1,multi2}.

\subsubsection{Glossaire}


Pour une meilleure lisibilité du mémoire, vous pouvez créer un
glossaire.
Il faut alors signaler par un astérisque les mots qui y sont définis
(une seule fois par mot).
Vous pouvez également utiliser des notes de bas de page (si vous avez
moins de dix mots concernés).

\subsubsection{Annexes}


Elles sont facultatives.
Ce sont des documents qui illustrent les informations données dans le
corps du mémoire.
Leur nombre maximal est fixé à 10.
Elles sont placées à la fin du rapport, sont précédées d’une table des
annexes (à moins qu'elles n'apparaissent directement dans la table des
matières), portent un titre, sont numérotées (mais la pagination n’est
pas imposée), leur source est mentionnée, leur classement correspond à
leur ordre d’apparition dans le mémoire et elles doivent être
signalées dans le mémoire.


\section{Soutenance}

La soutenance s'effectue devant un jury constitué d'enseignants,
\textbf{dont un d'entre eux peut ne pas être informaticien}, et le
responsable en entreprise s'il peut se rendre disponible.
C'est, comme pour toutes les soutenances, un \emph{exercice de
  style}.
Il faut réussir à restituer en un temps limité un grand nombre
d'informations.
Il y a donc des choix à faire...
Quels que soient ces choix, il faut :
\begin{itemize}
\item 
  une progression logique dans l'exposé (du plus général au plus
  spécifique),
\item
  des informations restituées avec du recul, permettant une
  compréhension minimale à une personne qui n'est pas experte du
  domaine de votre stage,
\item
  une mise en avant des aspects les plus importants de \emph{vos
    contributions} (n'oubliez pas que cette soutenance permet
  d'évaluer autant vos compétences, vos apports durant le stage que
  votre capacité à les restituer).
\end{itemize}

\medskip

Votre rapport doit \textbf{exhaustivement} décrire tout le travail
effectué durant votre stage.
Pour la soutenance, il est possible de s'écarter partiellement de
cette exhaustivité. 
En effet, pour \emph{certains stages} où de nombreuses tâches ont été
réalisées, il peut être intéressant de faire le choix de mettre en
avant vos \textbf{principales} contributions.
Cela offre l'avantage d'avoir plus de temps pour les décrire.
Dans ces situations, il ne faut pas pour autant occulter les autres
tâches : elles peuvent être regroupées sur un transparent où vous les
évoquerez rapidement.
Si vous n'êtes pas sûrs de vos choix, contactez votre tuteur qui
pourra vous assister.

\medskip

Voici quelques autres aspects factuels et techniques liés à la
soutenance :
\begin{itemize}
\item
  vous devez préparer un support de présentation numérique (type
  PowerPoint ou PDF),
\item 
  il y a 20 minutes de présentation et 10 minutes de questions du
  jury, 
\item
  au niveau du contenu (présentation et support), on doit trouver :
  \begin{itemize}
  \item 
    une présentation : lieu de votre stage, présentation de
    l’entreprise, du service concerné (le cas échéant), de ses
    besoins, lesquels doivent justifier votre sujet (à présenter),
  \item 
    la mission confiée : point de départ, objectifs, analyse, moyens
    mis en œuvre\,/\,réalisations décrivant l’évolution  de votre
    travail, les aboutissants (tests, testeurs, résultats,
    perspectives),
  \item 
    une conclusion : synthèse du travail réalisé, les apports
    (professionnels, personnels),
  \end{itemize}
\item
  attention au volume de transparents : cela dépend naturellement de
  ce qu'il contiennent, mais un transparent permet facilement de
  parler de 1 à 2 minutes,
\item
  répétez votre soutenance pour être sûr de votre discours et de votre
  timing,
\item
  indiquez en légende la source des figures que vous utilisez (si vous
  ne les avez pas faites vous-même),
\item
  ne prévoyez pas de notes écrites pour vous assister : une soutenance
  n'est jamais aussi agréable que quand l'orateur s'exprime de façon
  naturelle,
\item
  \textbf{et surtout} : ne vous faites pas «~voler la vedette~» par
  vos transparents.
  Ceux-ci sont un simple support, le jury attend surtout votre
  prestation orale.
  Une erreur classique ? Lire les transparents qui défilent alors que
  le jury les lit plus vite que vous et sera surtout plus à l'aise
  pour le faire...
\end{itemize}


\section{Fiche bibliothèque}

Cette fiche doit \textbf{obligatoirement} être insérée dans votre
rapport (typiquement à la fin).
En effet, tous les rapports qui reçoivent une évaluation supérieure à
$\frac{13}{20}$ sont déposés à la bibliothèque universitaire de l’IUT
(où ils peuvent y être consultés).
Il faut systématiquement anticiper cette possibilité puisque vous ne
pouvez pas connaître votre note à l'avance.
Vous devez fournir un certain nombre d'informations, dont deux
qui sont particulièrement importantes dans le contexte de la
documentation :
\begin{itemize}
\item 
  \textbf{les mots-clés} à partir desquels toute personne qui
  effectuera une recherche pourra accéder à votre rapport.
  Veillez donc à les sélectionner judicieusement ;
\item 
  \textbf{un résumé court} (synopsis de 3 à 5 lignes) qui renseigne
  sur le sujet et les principaux thèmes\,/\,aspects traités.
  Comme le sommaire, ce résumé doit renseigner sur les
  caractéristiques de votre sujet et du domaine exploré.
  On recherche donc ici la concision et la précision.
  Toute personne qui lit votre résumé doit comprendre si la lecture
  complète ou partielle de votre rapport a un intérêt pour elle.
\end{itemize}

\medskip

Le modèle de cette fiche bibliothèque est disponible sur l'ENT, dans
le cours «~Stages département informatique~», dans la section
«~Documents pour la fin du stage~».

\newpage

\appendix

\section*{Annexes}
\addcontentsline{toc}{section}{Annexes}


\section{Outils}

Vous avez la possibilité de choisir l'outil informatique de votre
choix pour réaliser votre rapport, à condition toutefois de vous
conformer aux consignes données dans le présent document.
Indépendamment des outils commerciaux, certains logiciels libres et
gratuits peuvent vous assister :
\begin{itemize}
\item
  Pour la rédaction de votre rapport, vous pouvez utiliser
  LibreOffice, qui est disponible sous toutes les plates-formes
  (Windows, Mac, Linux) : \url{https://fr.libreoffice.org/}.
\item
  Un correcteur orthographique est déjà présent sous Libreoffice.
  Si vous utilisez un autre outil (comme
  \LaTeX~\cite{Goossens:1994:LC:561206,lozano,wikilatex}),
  n'oubliez pas d'installer et d'utiliser un correcteur comme aspell :
  \url{http://aspell.net/}.
\item
  Toujours sous Libreoffice, il est possible d'utiliser Grammalecte
  (\url{https://www.dicollecte.org/grammalecte/}) pour la correction
  grammaticale.
  Cet outil est aussi disponible sous Emacs, toujours pour ceux qui
  composent leur rapport en \LaTeX.
\item
  Il existe des outils comme GanttProject
  (\url{http://www.ganttproject.biz/}) qui sont gratuits et
  disponibles pour toutes les plates-formes pour réaliser, justement,
  les diagrammes de Gantt.
\end{itemize}

\medskip

Toujours si vous utilisez un outil WYSIWYG, il est fortement conseillé
d'afficher les caractères spéciaux lors de votre rédaction
(correspondant généralement à l'icône \fbox{\faParagraph} dans la
barre de raccourcis).
Cela peut en particulier vous permettre de détecter les espaces
multiples, particulièrement disgracieuses dans un document.

En dernier ressort, si vous n'êtes pas sûr de votre composition,
n'hésitez pas à la faire relire par des personnes compétentes.
C'est le réflexe systématique qu'ont les professionnels de l'édition.
Il va sans dire que la question ne devrait donc même pas se poser pour
des personnes dont ça n'est pas le métier...


\section{Utilisation d'un traitement texte}
\label{sec:wys}

Si vous utilisez un traitement de texte WYSIWYG\footnote{\emph{What
    you see is what you get}.}, comme Word ou Libreoffice, vous pouvez
lui faire générer la table des matières automatiquement.
Il est pour cela nécessaire d'utiliser les \emph{styles} proposés par
ces logiciels.
Par défaut, ce style est généralement positionné à «~Normal~» ou
«~Style par défaut~».
L'utilisation de ces styles est primordiale pour atteindre une
homogénéité de présentation nécessaire pour ce type de rapports.

Sur le fond, ces styles apportent de la sémantique (du sens) en
permettant d'associer une étiquette logique (le «~style~» justement) à
chaque paragraphe défini.
Si chaque paragraphe est associé au «~bon~» style, il est ensuite
facile de modifier globalement la présentation du rapport en modifiant
\emph{directement} les mises en forme qui leurs sont associés.
Ainsi, si on change la police de caractères associée au style «~Corps
de document~», la répercussion se fera sur tous les paragraphes
relevant de ce style.

Autre avantage : des styles «~Titre~1~», «~Titre~2~», etc. sont
définis par défaut.
En les associant respectivement aux titres de premier niveau, deuxième
niveau, etc., le traitement de texte sera capable de générer (et
maintenir) une table des matières automatique.
Pour insérer une telle table :
\begin{enumerate}
\item
  placez le curseur à l'endroit d'insertion souhaité,
\item 
  localisez dans le menu ou le ruban «~Insertion~» (suivant le
  logiciel utilisé) la commande permettant de générer la table des
  matières.
  Celle-ci sera justement construite à partir des paragraphes associés
  aux styles «~Titre \emph{x}~».
\end{enumerate}


\section{Exemple de planning du stage}
\label{sec:gantt}


\begin{center}
  \includegraphics[angle=90,height=21cm]{stage.png}
\end{center}

\medskip

\begin{center}
  \emph{Diagramme réalisé avec GanttProject
    (\url{http://www.ganttproject.biz/}).}
\end{center}


\section{Erreurs fréquemment commises}

Il y en a un certain nombre, certaines directement liées aux consignes
rassemblées dans ce document, d'autres allant à l'encontre du «~bon
sens~».
On peut citer, de manière non exhaustive bien sûr :
\begin{itemize}
\item
  \textbf{Les rapports commençant par présenter la partie technique du
    projet}.\\
  On trouve parfois des rapports :
  \begin{itemize}
  \item
    Commençant par : «~L'objectif de ce stage était de faire un projet
    en PHP tournant sur un serveur Web~»\\
    \soluce
    Non, l'objectif d'un stage n'est \textbf{jamais} de faire
    un site en PHP.
    L'objectif est de répondre à un besoin, d'implanter un service.
    Après avoir décrit cet objectif, analysé les tenants et les
    aboutissants, on peut\,/\,on doit indiquer qu'on a utilisé PHP pour
    le faire.
    Mais ça ne reste qu'un moyen d'arriver à répondre au besoin
    initial.
  \item
    Entrant quasiment immédiatement dans la description du travail
    réalisé.\\
    \soluce
    Un rapport est sensé se lire linéairement, de la première à la
    dernière page.
    Si le lecteur est obligé de papillonner à la lecture du rapport,
    c'est que son organisation est maladroite.
    Le principe, expliqué tout au long de ces consignes, est pourtant
    simple : on part du plus général (l'entreprise, le service
    d'accueil, les projets de ce service) pour aller vers le plus
    particulier (les objectifs du stage, l'analyse faite, la
    réalisation, les résultats).
  \end{itemize}
\item
  \textbf{Les rapports contenant des fautes d'\emph{orthographe}}.\\
  \soluce
  Une faute d'orthographe est souvent le fait d'utiliser un mot qui
  n'existe pas dans le dictionnaire (ex : un «~akord~»,
  «~féblement~», etc.)
  Sur ces exemples, les outils de correction automatique, qui sont
  maintenant universellement accessibles, ne peuvent que déclencher
  une alerte.
  Le lecteur (en premier lieu, celui qui va noter le rapport) trouvant
  une faute de ce type ne peut conclure qu'au fait que le rédacteur
  n'a pas fait son travail sérieusement.
  Et, très logiquement, l'évaluation s'en ressentira.
\item
  \textbf{Les rapports utilisant de nombreux temps pour la conjugaison
    des verbes}.\\
  \soluce
  Ce type d'approche est souvent très mal maîtrisée, quand elle n'est
  pas tout simplement inadéquate.
  Le rapport de stage est avant tout un document technique, très
  factuel.
  Le plus simple est de s'astreindre à utiliser essentiellement le
  présent et de façon marginale d'autres temps de l'indicatif lorsque
  cela est pertinent.
\item
  \textbf{Les rapports utilisant des synonymes pour des termes
    techniques consacrés}.\\
  \soluce
  Si on apprend très tôt en rédaction que les répétitions alourdissent
  le style d'un document, c'est souvent une erreur de chercher à
  trouver des synonymes pour des termes qui ne s'y prêtent pas.
  Inutile donc, et même contre-productif, de chercher à caser
  «~logiciel~», «~\emph{soft}~», «~programme~», «~progiciel~»,
  etc. dans une même phrase si on parle de la même chose.
  On va juste réussir à perdre le lecteur qui va finir par se demander
  si tous ces termes sont finalement relatifs au même concept.
  Encore une fois, le rapport est essentiellement un document
  technique.
  Il convient d'utiliser les bons termes lors des explications (il
  reste la latitude de chercher des synonymes sur les mots non
  techniques bien sûr~!)
\item
  \textbf{Les rapports non homogènes au niveau de l'expression}.\\
  Cela peut recouvrir des points très différents.
  À vrai dire, sur certains aspects, le rédacteur a le choix d'opter
  pour un style ou un autre, mais il faut ensuite qu'il s'y tienne.
  En particulier :
  \begin{itemize}
  \item
    L'usage du «~je~», du «~nous~», du «~on~».\\
    \soluce
    Il faut choisir son camp~! Une remarque tout de même : il est
    possible d'utiliser conjointement le «~je~» et le «~on~» par
    exemple.
    On réservera alors l'usage du premier aux actions réalisées
    purement par l'auteur et l'usage du second aux actions réalisées
    par l'équipe entière.
  \item
    Les rapports présentant des parties très rédigées, presque
    littéraires, alors que d'autres ne sont que des énumérations
    brutes, comme des listes à puces, sans liaison.\\
    \soluce
    Même si des parties plus techniques peuvent avantageusement être
    rédigées avec des énumérations, il est de la responsabilité du
    rédacteur d'assurer la continuité des idées qu'il expose.
  \end{itemize}
\item
  \textbf{L'utilisation de trop nombreux \emph{screenshots} (images
    écran)}.\\
  Dans certains rapports, ces images peuvent occuper une dizaine de
  pages pour présenter simplement la progression d'un dialogue avec
  l'utilisateur.\\
  \soluce
  S'il est utile, voire parfois nécessaire, d'inclure de telles
  images, elles doivent absolument servir le discours, être associées
  à une légende, être référencées dans le rapport, être commentées et
  être discutées.
  Elles ne sont d'aucun intérêt si elles ne servent qu'à «~remplir
  l'espace~».
  Dans quelques cas particuliers (avec un sujet de stage basé
  justement sur la définition d'interfaces utilisateur par exemple),
  il peut être utile d'en insérer certaines dans le document principal
  et de disposer les autres en annexe.
\end{itemize}

\vfil

\begin{center}
  \textbf{\large Vous avez fini la rédaction de votre rapport ?
    Vous vous apprêtez à le rendre ?
    Merci de relire ces consignes et de vérifier que toutes les
    contraintes qui vous ont été données ont bien été respectées.
    Chaque contrainte est considérée dans l'évaluation...}
\end{center}

\vfil

\newpage

\def\refname{Bibliographie}
\bibliographystyle{apalike-fr}
\bibliography{notice_stages}
\addcontentsline{toc}{section}{Bibliographie}

\vfill

{\footnotesize
  Document réalisé sous Linux avec Emacs et compilé grâce à
  \XeLaTeX.
  Document publié sous licence \textbf{Art Libre}
  (\url{http://artlibre.org/}).
  Sources de ce document librement accessibles sur
  \url{https://bitbucket.org/depinfoens/stages}.
}

%======================================================================
% F I N   D U   D O C U M E N T
%======================================================================

\end{document}
